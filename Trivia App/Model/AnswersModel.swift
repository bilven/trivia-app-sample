//
//  AnswersModel.swift
//  Trivia App
//
//  Created by Bilven Parikh on 16/03/19.
//  Copyright © 2019 Bilven Parikh. All rights reserved.
//

import UIKit

class AnswersModel: NSObject {
    
    var answers : [Answer] = []
    static var modelClass : AnswersModel? = nil
    
    class func sharedModel() -> AnswersModel{
        if modelClass == nil {
            modelClass = AnswersModel()
        }
        return modelClass!
    }
    
    //Function returns array answers of given question id
    func getAnswers(_ questionId:Int) -> [Answer] {
        answers = getAnswers()
        let answersOfQuestionId = answers.filter({$0.questionId == questionId})
        return answersOfQuestionId
    }
    
    //get all answers
    func getAnswers() -> [Answer]{
        var answers : [Answer] = []
        let arrAnwers = AppDelegate.sharedInstance().skdb.lookupAll(forSQL: String(format:"select * from AnswersList")) as NSArray
        arrAnwers.forEach { (dictionary) in
            let objAnswer = Answer.init(fromDictionary: dictionary as? NSDictionary)
            answers.append(objAnswer)
        }
        return answers
    }
}
