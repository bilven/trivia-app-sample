//
//  HistoryModel.swift
//  Trivia App
//
//  Created by Bilven Parikh on 16/03/19.
//  Copyright © 2019 Bilven Parikh. All rights reserved.
//

import UIKit

class HistoryModel: NSObject {
    
    var history : [History] = []
    
    static var modelClass : HistoryModel? = nil
    
    class func sharedModel() -> HistoryModel{
        if modelClass == nil {
            modelClass = HistoryModel()
        }
        return modelClass!
    }
    
    //get history data given by userid
    func getHistory(_ userId:Int) -> [History] {
        history = getHistory()
        let arrHistory = history.filter({$0.userId == userId})
        return arrHistory
    }
    
    //get all history data from database
    func getHistory() -> [History]{
        var history : [History] = []
        let arrHistory = AppDelegate.sharedInstance().skdb.lookupAll(forSQL: String(format:"select * from GamesPlayed")) as NSArray
        arrHistory.forEach { (dictionary) in
            let objHistory = History.init(fromDictionary: dictionary as? NSDictionary)
            history.append(objHistory)
        }
        return history
    }
    
    //add game to history
    func addGameToHistory(_ arrQuestions : [Question]) {
        arrQuestions.forEach { (question) in
            let dictionary = NSMutableDictionary()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd MMM yyyy hh:mm:ss"
            let result = formatter.string(from: Date())
            dictionary["playedon"] = "\(result)"
            dictionary["question"] = question.questionText
            if question.hasMutliSelect{
                let answer = question.answers.filter({$0.isSelected}).map{$0.answerText}.joined(separator: ",")
                dictionary["answer"] = answer.count > 0 ? answer : "Not Answered"
            }else{
                dictionary["answer"] = question.answers.filter({$0.isSelected}).first?.answerText ?? "Not Answered"
            }
            let objUser = AppDelegate.sharedInstance().skdb.lookupAll(forSQL: "select * from Users")?.last as! NSDictionary
            dictionary["userid"] = objUser["userid"] ?? 0
            AppDelegate.sharedInstance().skdb.insert((dictionary as! [AnyHashable : Any]), forTable: "GamesPlayed")
        }
    }

}
