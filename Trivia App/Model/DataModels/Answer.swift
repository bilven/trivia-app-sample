//
//  Answer.swift
//  Trivia App
//
//  Created by Bilven Parikh on 16/03/19.
//  Copyright © 2019 Bilven Parikh. All rights reserved.
//

import UIKit

//Answer Object
class Answer: NSObject {
    var answerId : Int!
    var questionId : Int!
    var answerText : String!
    var isSelected : Bool = false
    
    //Initialization of Answer
    override init() {
        
    }
    
    //Initialization from Dictionary
    init(fromDictionary dictionary: NSDictionary!){
        answerId = dictionary["answerId"] as? Int
        questionId = dictionary["questionId"] as? Int
        answerText = dictionary["answerText"] as? String
    }
}
