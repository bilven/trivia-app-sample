//
//  Question.swift
//  Trivia App
//
//  Created by Bilven Parikh on 16/03/19.
//  Copyright © 2019 Bilven Parikh. All rights reserved.
//

import UIKit

//Question Object
class Question: NSObject {
    var questionId : Int!
    var questionText : String!
    var hasMutliSelect : Bool!
    //array of answers for question
    var answers : [Answer] = []
    
    override init() {
        
    }
    //Initialization of Question
    init(fromDictionary dictionary: NSDictionary!){
        hasMutliSelect = dictionary["hasMutliSelect"] as? Bool
        questionId = dictionary["questionId"] as? Int
        questionText = dictionary["questionText"] as? String
        answers = AnswersModel.sharedModel().getAnswers(questionId)
    }
    
}
