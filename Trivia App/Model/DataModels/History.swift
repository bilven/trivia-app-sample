//
//  History.swift
//  Trivia App
//
//  Created by Bilven Parikh on 16/03/19.
//  Copyright © 2019 Bilven Parikh. All rights reserved.
//

import UIKit

//History Object to show previously played games
class History: NSObject {
    var gameId : Int!
    var playedOn : String!
    var userId : Int!
    var question : String!
    var answer : String!
    
    //Initialization from Dictionary
    init(fromDictionary dictionary: NSDictionary!){
        gameId = dictionary["gameid"] as? Int
        userId = dictionary["userid"] as? Int
        playedOn = dictionary["playedon"] as? String
        question = dictionary["question"] as? String
        answer = dictionary["answer"] as? String
    }
}
