//
//  User.swift
//  Trivia App
//
//  Created by Bilven Parikh on 16/03/19.
//  Copyright © 2019 Bilven Parikh. All rights reserved.
//

import UIKit
//User object who played
class User: NSObject {
    var userId : Int!
    var userName : String!
    //array of questions and answers given by user
    var history : [History] = []
    
    override init() {
        
    }
    
    init(fromDictionary dictionary: NSDictionary!){
        userId = dictionary["userid"] as? Int
        userName = dictionary["username"] as? String
        history = HistoryModel.sharedModel().getHistory(dictionary["userid"] as? Int ?? 0)
    }
}
