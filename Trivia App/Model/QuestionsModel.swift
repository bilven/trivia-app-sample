//
//  QuestionModel.swift
//  Trivia App
//
//  Created by Bilven Parikh on 16/03/19.
//  Copyright © 2019 Bilven Parikh. All rights reserved.
//

import UIKit

class QuestionsModel: NSObject {
    
    static var modelClass : QuestionsModel? = nil
    
    class func sharedModel() -> QuestionsModel{
        if modelClass == nil {
            modelClass = QuestionsModel()
        }
        return modelClass!
    }
    
    //Function return Array of questions from DB
    func getQuestions() -> [Question] {
        var questions : [Question] = []
        let arrQuestion = AppDelegate.sharedInstance().skdb.lookupAll(forSQL: String(format:"select * from QuestionList")) as NSArray
        arrQuestion.forEach { (dictionary) in
            let objQuestion : Question = Question.init(fromDictionary: dictionary as? NSDictionary)
            questions.append(objQuestion)
        }
        return questions
    }
}
