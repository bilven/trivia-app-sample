//
//  UserModel.swift
//  Trivia App
//
//  Created by Bilven Parikh on 16/03/19.
//  Copyright © 2019 Bilven Parikh. All rights reserved.
//

import UIKit

class UserModel: NSObject {
    
    static var modelClass : UserModel? = nil
    
    class func sharedModel() -> UserModel{
        if modelClass == nil {
            modelClass = UserModel()
        }
        return modelClass!
    }
    
    //get users list who played games
    func getUsersListWithGamesPlayed() -> [User] {
        var users : [User] = []
        let arrQuestion = AppDelegate.sharedInstance().skdb.lookupAll(forSQL: String(format:"select * from Users")) as NSArray
        arrQuestion.forEach { (dictionary) in
            let objUser : User = User.init(fromDictionary: dictionary as? NSDictionary)
            objUser.history.count > 0 ? users.append(objUser) : print("Not Played any game")
        }
        return users
    }
    
    //add user to db
    func addUser(_ userName : String) -> User{
        AppDelegate.sharedInstance().skdb.insert(["username":"\(userName)"], forTable: "Users")
        let objUser = AppDelegate.sharedInstance().skdb.lookupAll(forSQL: "select * from Users")?.last as! NSDictionary
        let user = User.init(fromDictionary: objUser)
        return user
    }
    
}
