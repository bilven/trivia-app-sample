//
//  QuestionCollectionViewCell.swift
//  Trivia App
//
//  Created by Bilven Parikh on 16/03/19.
//  Copyright © 2019 Bilven Parikh. All rights reserved.
//

import UIKit

class QuestionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var collectionAnswer: UICollectionView!
    @IBOutlet weak var lblQuestion: UILabel!
}
