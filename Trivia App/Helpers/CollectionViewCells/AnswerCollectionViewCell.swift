//
//  AnswerCollectionViewCell.swift
//  Trivia App
//
//  Created by Bilven Parikh on 16/03/19.
//  Copyright © 2019 Bilven Parikh. All rights reserved.
//

import UIKit

class AnswerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblAnswer: UILabel!
    @IBOutlet weak var imgTick: UIImageView!
    
}
