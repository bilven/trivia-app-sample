//
//  QuestionAnswerViewController.swift
//  Trivia App
//
//  Created by Bilven Parikh on 16/03/19.
//  Copyright © 2019 Bilven Parikh. All rights reserved.
//

import UIKit

class QuestionAnswerViewController: UIViewController, UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionQuestions: UICollectionView!
    var user : User!
    var questions : [Question] = []
    var answers : [Answer] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        questions = QuestionsModel.sharedModel().getQuestions()
        collectionQuestions.reloadData()
        self.navigationItem.hidesBackButton = true
        self.title = "Answer below questions."
        // Do any additional setup after loading the view.
    }
    
    //Next Question or Finish
    @IBAction func btnNext_Tapped(_ sender: Any) {
        let btn : UIButton = sender as! UIButton
        if btn.titleLabel?.text == "NEXT"{
            //next question
            self.collectionQuestions.scrollToItem(at:IndexPath(item: (answers.first?.questionId)!, section: 0), at: .right, animated: true)
        }else{
            //store game to histroy and move to summary controller
            HistoryModel.sharedModel().addGameToHistory(questions)
            let summaryVC : SummaryViewController = self.storyboard?.instantiateViewController(withIdentifier: "SummaryViewController") as! SummaryViewController
            summaryVC.user = self.user
            summaryVC.questions = self.questions
            self.navigationController?.pushViewController(summaryVC, animated: true)
        }
    }
    
// MARK: - Collection datasource and delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionQuestions{
            return questions.count
        }else{
            return answers.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collectionQuestions{
            return self.collectionQuestions!.bounds.size
        }else{
            return CGSize.init(width: (UIScreen.main.bounds.size.width / 2) - 20, height: 100)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionQuestions{
            let cell : QuestionCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuestionCollectionViewCell", for: indexPath) as! QuestionCollectionViewCell
            cell.lblQuestion.text = questions[indexPath.item].questionText
            self.answers = questions[indexPath.item].answers
            cell.collectionAnswer.reloadData()
            cell.btnNext.setTitle(indexPath.item == questions.count-1 ? "FINISH" : "NEXT", for: .normal)
            return cell
        }else{
            let cell : AnswerCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnswerCollectionViewCell", for: indexPath) as! AnswerCollectionViewCell
            cell.lblAnswer.text = answers[indexPath.item].answerText
            cell.imgTick.layer.borderColor = UIColor.black.cgColor
            cell.imgTick.layer.borderWidth = 1.5
            cell.imgTick.image = answers[indexPath.item].isSelected ? #imageLiteral(resourceName: "ic_tick_new") : UIImage.init()
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView != collectionQuestions{
            let index = collectionQuestions.indexPathsForVisibleItems
            if questions[index.first!.item].hasMutliSelect{
                answers[indexPath.item].isSelected = !answers[indexPath.item].isSelected
            }else{
                answers.forEach({$0.isSelected = false})
                answers[indexPath.item].isSelected = true
            }
            collectionView.reloadData()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
