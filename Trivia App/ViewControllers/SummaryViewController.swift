//
//  SummaryViewController.swift
//  Trivia App
//
//  Created by Bilven Parikh on 16/03/19.
//  Copyright © 2019 Bilven Parikh. All rights reserved.
//

import UIKit

class SummaryViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tblQuestionAnswers: UITableView!
    @IBOutlet weak var lblUserName: UILabel!
    var user : User!
    var questions : [Question] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblQuestionAnswers.tableFooterView = UIView.init()
        self.navigationItem.hidesBackButton = true
        self.title = "Your Answers"
        lblUserName.text = "Hello : "+user.userName
        // Do any additional setup after loading the view.
    }
    
    // MARK: - TableView datasource and delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : SummaryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SummaryTableViewCell") as! SummaryTableViewCell
        cell.selectionStyle = .none
        cell.lblQuestion.text = questions[indexPath.row].questionText
        if questions[indexPath.row].hasMutliSelect{
            let answer = questions[indexPath.row].answers.filter({$0.isSelected}).map{$0.answerText}.joined(separator: ",")
            cell.lblAnswer.text = answer.count > 0 ? "Answer : "+answer : "Answer : Not Answered"
        }else{
            cell.lblAnswer.text = "Answer : "+(questions[indexPath.row].answers.filter({$0.isSelected}).first?.answerText ?? "Not Answered")
        }
        return cell
    }
    
    //FINISH and move to root controller
    @IBAction func btnFinish_Tapped(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //Show history of games
    @IBAction func btnHistory_Tapped(_ sender: Any) {
        let historyVC : HistoryViewController = self.storyboard?.instantiateViewController(withIdentifier: "HistoryViewController") as! HistoryViewController
        self.navigationController?.pushViewController(historyVC, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
