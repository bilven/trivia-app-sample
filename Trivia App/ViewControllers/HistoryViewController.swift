//
//  HistoryViewController.swift
//  Trivia App
//
//  Created by Bilven Parikh on 16/03/19.
//  Copyright © 2019 Bilven Parikh. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var lblGameTitle: UILabel!
    
    var users : [User] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        users = UserModel.sharedModel().getUsersListWithGamesPlayed()
        self.title = "Games Played!"
        // Do any additional setup after loading the view.
    }
    
    // MARK: - TableView datasource and delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.tableFooterView = UIView.init()
        let cell : HistoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell") as! HistoryTableViewCell
        cell.selectionStyle = .none
        cell.lblUserName.text = users[indexPath.row].userName
        cell.lblGameTitle.text = "Game \(indexPath.row+1) : Played on - \(users[indexPath.row].history.first?.playedOn ?? "\(Date())")"
        cell.lblQuestionAnswer.text = users[indexPath.row].history.map{$0.question+"\nAnswer : "+$0.answer}.joined(separator: "\n")
        return cell
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
