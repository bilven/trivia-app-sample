//
//  HomeViewController.swift
//  Trivia App
//
//  Created by Bilven Parikh on 16/03/19.
//  Copyright © 2019 Bilven Parikh. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet var tfUserName : UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tfUserName.text = ""
    }
    
    //Start Game
    @IBAction func btnNext_Tapped(_ sender : UIButton){
        let user = UserModel.sharedModel().addUser((tfUserName.text?.count)! > 0 ? tfUserName.text! : "Anonymous")
        let QAVC : QuestionAnswerViewController = self.storyboard?.instantiateViewController(withIdentifier: "QuestionAnswerViewController") as! QuestionAnswerViewController
        QAVC.user = user
        self.navigationController?.pushViewController(QAVC, animated: true)
    }
    
    //TextField delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
